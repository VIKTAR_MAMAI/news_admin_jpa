package com.epam.newsadmin.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.web.context.ConfigurableWebApplicationContext;

public class AppInitializer implements ApplicationContextInitializer<ConfigurableWebApplicationContext>  {

	private static final Logger LOG = Logger.getLogger(AppInitializer.class);
	
	public void initialize(ConfigurableWebApplicationContext ctx) {
		try {
			addPropertySource("fileLocations", "/util.properties", ctx);
		} catch (IOException e) {
			throw new IllegalArgumentException("Error loading context properties", e);
		}
	}

	private void addPropertySource(String beanName, String fileName, ConfigurableWebApplicationContext ctx) throws IOException {
		LOG.info("Loading properties " + fileName + " into property source " + beanName);
		InputStream input = AppInitializer.class.getResourceAsStream(fileName);
		try {
			Properties props = new Properties();
			props.load(input);
			PropertiesPropertySource propSource = new PropertiesPropertySource(beanName, props);
			
			ctx.getEnvironment().getPropertySources().addFirst(propSource);
		} finally {
			input.close();
		}
	}

}
