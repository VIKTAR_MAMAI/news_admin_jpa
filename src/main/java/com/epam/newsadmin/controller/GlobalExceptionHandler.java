package com.epam.newsadmin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsadmin.exception.ControllerException;

@ControllerAdvice
public class GlobalExceptionHandler {
	private static Logger LOG = Logger.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(value = { ControllerException.class })
	public ModelAndView handleException(HttpServletRequest request, Exception ex) {
		LOG.error(ex);
		ModelAndView modelAndView = new ModelAndView("error/controller");
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return modelAndView;
	}
}
