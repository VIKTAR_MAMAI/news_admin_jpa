package com.epam.newsadmin.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.epam.newsadmin.exception.ControllerException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.IAuthorService;
import com.epam.newscommon.service.INewsService;
import com.epam.newscommon.service.ITagService;
import com.epam.newscommon.valueobject.FilteredItem;
import com.epam.newscommon.valueobject.NewsPageItem;

@Controller
@SessionAttributes(value = { ListNewsController.FILTERED_ITEM,
		ListNewsController.SOURCE_PAGE })
public class ListNewsController {
	public final static String FILTERED_ITEM = "filteredItem";
	public final static String SOURCE_PAGE = "sourcePage";

	@Autowired
	private INewsService newsService;
	@Autowired
	private ITagService tagService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private MessageSource messageSource;

	private final int NEWS_PER_PAGE = 6;

	@RequestMapping(value = { "/news/page/{page}" }, method = RequestMethod.GET)
	public ModelAndView loadAll(
			@PathVariable(value = "page") Integer pageNumber,
			ModelAndView model, HttpServletRequest request)
			throws ControllerException {
		try {
			FilteredItem filteredItem = new FilteredItem(null, null);
			model.addObject(FILTERED_ITEM, filteredItem);
			model.setViewName("news/list");

			NewsPageItem item = newsService.loadByFilter(filteredItem,
					pageNumber, NEWS_PER_PAGE);
			prepareModel(model, request, item);
			model.addObject(SOURCE_PAGE, "/news/page/" + pageNumber);
			return model;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/filter", method = RequestMethod.POST)
	public String loadByFilter(
			Model model,
			@RequestParam(value = "tagId", required = false) List<Long> tagIdList,
			@RequestParam(value = "authorId", required = false) Long authorId) {
		model.addAttribute(FILTERED_ITEM,
				new FilteredItem(tagIdList, authorId));
		return "redirect:/news/filter/1";
	}

	@RequestMapping(value = "/news/filter/{page}", method = { RequestMethod.GET })
	public ModelAndView loadByFilterPageable(
			@ModelAttribute("filteredItem") FilteredItem filteredItem,
			@PathVariable(value = "page") Integer pageNumber,
			HttpServletRequest request) throws ControllerException {
		try {
			ModelAndView model = new ModelAndView("news/list");
			NewsPageItem item = newsService.loadByFilter(filteredItem,
					pageNumber, NEWS_PER_PAGE);
			prepareModel(model, request, item);
			model.addObject("isFilter", true);
			model.addObject(SOURCE_PAGE, "/news/filter/" + pageNumber);
			return model;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	@RequestMapping(value = "/news/delete", method = RequestMethod.POST)
	public String delete(
			@RequestParam(value = "newsId", required = false) List<Long> newsIdList,
			@RequestParam(value = "pageNumber") Integer pageNumber,
			RedirectAttributes ra, HttpServletRequest request)
			throws ControllerException {
		try {
			newsService.deleteList(newsIdList);
			Locale locale = RequestContextUtils.getLocale(request);
			ra.addFlashAttribute("successMessage", messageSource.getMessage(
					"message.news.delete", new Object[] { newsIdList.size() },
					locale));
			return "redirect:/news/page/" + pageNumber;
		} catch (ServiceException e) {
			throw new ControllerException(e);
		}
	}

	private void prepareModel(ModelAndView model, HttpServletRequest request,
			NewsPageItem item) throws ServiceException {
		Locale locale = RequestContextUtils.getLocale(request);
		if (item.getNewsList().isEmpty()) {
			model.addObject("errorEmptyMessage", messageSource.getMessage(
					"message.news.empty", null, locale));
		}
		model.addObject("newsItem", item);
		model.addObject("tagList", tagService.loadAll());
		model.addObject("authorList", authorService.loadAll());
	}
}
